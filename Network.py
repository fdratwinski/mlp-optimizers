import math

import numpy as np


class Network:

    def __init__(self, num_of_inputs, num_of_neurons_hidden, num_of_classes,
                 activation_functions, optimizer):
        self.num_of_classes = num_of_classes
        self.activation_functions = activation_functions
        self.optimizer = optimizer

        self.W_list = []
        self.b_list = []

        self.W_adagard_acc_list = []
        self.b_adagrad_acc_list = []

        self.num_of_neurons_hidden = num_of_neurons_hidden
        # value = 2/(num_of_inputs)
        value = num_of_neurons_hidden[0] ** (1./ num_of_inputs)
        self.W_list.append(
            np.random.uniform((-1)*value, value, (num_of_inputs, num_of_neurons_hidden[0])))

        self.W_adagard_acc_list.append(np.zeros(shape=(num_of_inputs, num_of_neurons_hidden[0])))

        for i in range(len(num_of_neurons_hidden) - 1):
            value = num_of_neurons_hidden[0] ** (1./ num_of_neurons_hidden[i])
            # value = 2/(num_of_neurons_hidden[i])
            self.W_list.append(np.random.uniform((-1)*value, value,
                                                 (num_of_neurons_hidden[i], num_of_neurons_hidden[i + 1])))
            self.b_list.append(np.zeros(num_of_neurons_hidden[i]))

            self.W_adagard_acc_list.append(np.zeros(shape=(num_of_neurons_hidden[i], num_of_neurons_hidden[i + 1])))
            self.b_adagrad_acc_list.append(np.zeros(num_of_neurons_hidden[i]))

        value = num_of_neurons_hidden[0] ** (1./ num_of_neurons_hidden[-1])
        # value = 2/(num_of_neurons_hidden[-1])
        self.W_list.append(
            np.random.uniform((-1)*value, value, (num_of_neurons_hidden[-1], num_of_classes)))
        self.b_list.append(np.zeros(num_of_neurons_hidden[-1]))
        self.b_list.append(np.zeros(num_of_classes))

        self.W_adagard_acc_list.append(np.zeros(shape=(num_of_neurons_hidden[-1], num_of_classes)))
        self.b_adagrad_acc_list.append(np.zeros(num_of_neurons_hidden[-1]))
        self.b_adagrad_acc_list.append(np.zeros(num_of_classes))

        self.outputs = []
        self.impulses = []
        self.best_loss = None
        self.bestW = None
        self.bestB = None

    def forward_pass_step(self, weights, bias, inputs, activation_func):
        full_impulse = inputs @ weights + bias
        return activation_func.calculate_activation(full_impulse), full_impulse

    def forward_pass(self, data):
        self.outputs.clear()
        self.impulses.clear()
        output, impulse = self.forward_pass_step(self.W_list[0], self.b_list[0], data, self.activation_functions[0])
        self.outputs.append(output)
        self.impulses.append(impulse)
        for i in range(1, len(self.W_list)):
            output, impulse = self.forward_pass_step(self.W_list[i], self.b_list[i], self.outputs[-1],
                                                     self.activation_functions[i])
            self.outputs.append(output)
            self.impulses.append(impulse)

        return np.argmax(self.outputs[-1], axis=1)

    def back_propagation(self, data, actual_labels):
        true_labels_one_hot = self.to_one_hot_encoding(actual_labels, self.num_of_classes)
        shape = data.shape[0]
        deltas = []

        delta = self.activation_functions[-1].calculate_derivation(self.impulses[-1]) * (
                    true_labels_one_hot - self.outputs[-1])
        deltas.append(delta)
        self.update_weights(delta, -1, shape, self.outputs[-2])

        for i in range(1, len(self.W_list) - 1):
            delta = self.activation_functions[-1 - i].calculate_derivation(self.impulses[-1 - i]) * (
                        deltas[-1] @ self.W_list[-1 * i].T)
            deltas.append(delta)
            self.update_weights(delta, -1 - i, shape, self.outputs[-2 - i])

        delta = self.activation_functions[0].calculate_derivation(self.impulses[0]) * (deltas[-1] @ self.W_list[1].T)
        deltas.append(delta)
        self.update_weights(delta, 0, shape, data)

    def update_weights(self, delta, index, shape, output):
        self.optimizer.update_weights(self.W_list, self.b_list, delta, index, shape, output)

    def calculate_accuracy_and_loss(self, x, y):
        predicted_labels = self.forward_pass(x)
        last_output = self.outputs[-1]
        loss = np.sum((last_output - self.to_one_hot_encoding(y, self.num_of_classes)) ** 2) / x.shape[0]
        return np.sum(predicted_labels == y) / len(y), loss

    def check_for_best_loss(self, loss):
        if (self.best_loss is not None and loss < self.best_loss) or (self.best_loss is None):
            print(f"\tSaving best weights. Best loss was {self.best_loss}, new loss is {loss}")
            self.best_loss = loss
            self.bestW = self.W_list.copy()
            self.bestB = self.b_list.copy()

    def to_one_hot_encoding(self, labels, num_of_classes):
        labels_one_hot = np.zeros(shape=(labels.shape[0], num_of_classes))
        for i in range(len(labels)):
            labels_one_hot[i][labels[i]] = 1
        return labels_one_hot
