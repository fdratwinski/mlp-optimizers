class ActivationFunction:
    def __init__(self):
        pass

    def calculate_activation(self, x):
        return NotImplemented

    def calculate_derivation(self, x):
        return NotImplemented