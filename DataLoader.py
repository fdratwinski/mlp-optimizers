import numpy as np
import pickle as pkl


def load_data(pkl_path):
    with open(pkl_path, 'rb') as pickle_file:
        data = pkl.load(pickle_file, encoding='latin1')

    training_data = data[0][0]
    training_labels = data[0][1]
    validation_data = data[1][0]
    validation_labels = data[1][1]
    testing_data = data[2][0]
    testing_labels = data[2][1]

    num_of_classes = np.max(training_labels) + 1

    return (training_data, training_labels), (validation_data, validation_labels), (
    testing_data, testing_labels), num_of_classes
