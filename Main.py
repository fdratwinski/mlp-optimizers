import DataLoader
import numpy as np

from Network import Network
from activation_functions.ReLU import ReLU
from activation_functions.Sigmoid import Sigmoid
from optimizers.AdaDelta import AdaDelta
from optimizers.AdaGrad import AdaGrad
from optimizers.Adam import Adam
from optimizers.Momentum import Momentum
from optimizers.SGD import SGD

np.random.seed(127)


def train(model, x_t, y_t, x_v, y_v, batch_size, epochs, learning_rate):
    header_string = "Training accuracy;Training loss;Validation accuracy;Validation loss"
    csv_lines = [header_string]
    for i in range(epochs):
        for j in range(0, len(y_t), batch_size):
            model.forward_pass(x_t[j:j + batch_size])
            model.back_propagation(x_t[j:j + batch_size], y_t[j:j + batch_size])
        training_accuracy, training_loss = model.calculate_accuracy_and_loss(x_t, y_t)
        validation_accuracy, validation_loss = model.calculate_accuracy_and_loss(x_v, y_v)
        print(
            f"Epoch no. {i} - training accuracy: {training_accuracy}, training loss: {training_loss}, validation accuracy {validation_accuracy}, validation loss {validation_loss}")
        model.check_for_best_loss(validation_loss)
        csv_lines.append(str(training_accuracy) + ";" + str(training_loss) + ";" + str(validation_accuracy) + ";" + str(
            validation_loss))
    save_to_csv(csv_lines, model.optimizer.get_name() + "LR=" + str(learning_rate) + "SECOND.csv")


def save_to_csv(lines, name):
    with open("results/" + name, 'w') as file:
        for line in lines:
            file.write(line)
            file.write('\n')


if __name__ == '__main__':
    training, validation, testing, num_of_classes = DataLoader.load_data('data/mnist.pkl')
    num_of_inputs = training[0].shape[1]

    epochs_for_all = 50
    activation_functions = [[ReLU(), Sigmoid()]]

    batch_size = 50
    num_of_neruons_hidden = 200

    learning_rates = [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.05, 0.1]

    # SGD
    for lr in learning_rates:
        network = Network(num_of_inputs,
                          [num_of_neruons_hidden],
                          num_of_classes,
                          activation_functions[0],
                          SGD(
                              learning_rate=lr,
                              num_of_inputs=num_of_inputs,
                              num_of_neurons_hidden=[num_of_neruons_hidden],
                              num_of_classes=num_of_classes))

        train(network, training[0], training[1], validation[0], validation[1], batch_size=batch_size, epochs=epochs_for_all, learning_rate=lr)

    # Momentum
    for lr in learning_rates:
        network = Network(num_of_inputs,
                          [num_of_neruons_hidden],
                          num_of_classes,
                          activation_functions[0],
                          Momentum(
                              learning_rate=lr,
                              gamma=0.9,
                              num_of_inputs=num_of_inputs,
                              num_of_neurons_hidden=[num_of_neruons_hidden],
                              num_of_classes=num_of_classes))

        train(network, training[0], training[1], validation[0], validation[1], batch_size=batch_size, epochs=epochs_for_all, learning_rate=lr)

    # AdaGrad
    for lr in learning_rates:
        network = Network(num_of_inputs,
                          [num_of_neruons_hidden],
                          num_of_classes,
                          activation_functions[0],
                          AdaGrad(
                              learning_rate=lr,
                              epsilon=10e-8,
                              num_of_inputs=num_of_inputs,
                              num_of_neurons_hidden=[num_of_neruons_hidden],
                              num_of_classes=num_of_classes))

        train(network, training[0], training[1], validation[0], validation[1], batch_size=batch_size, epochs=epochs_for_all, learning_rate=lr)

    # AdaDelta
    network = Network(num_of_inputs,
                      [num_of_neruons_hidden],
                      num_of_classes,
                      activation_functions[0],
                      AdaDelta(
                          learning_rate=0.1,
                          gamma=0.9,
                          epsilon=10e-8,
                          num_of_inputs=num_of_inputs,
                          num_of_neurons_hidden=[num_of_neruons_hidden],
                          num_of_classes=num_of_classes))

    train(network, training[0], training[1], validation[0], validation[1], batch_size=batch_size, epochs=epochs_for_all, learning_rate=0.1)


    # Adam
    for lr in learning_rates:
        network = Network(num_of_inputs,
                          [num_of_neruons_hidden],
                          num_of_classes,
                          activation_functions[0],
                          Adam(
                              learning_rate=lr,
                              beta1=0.9,
                              beta2=0.999,
                              epsilon=10e-8,
                              num_of_inputs=num_of_inputs,
                              num_of_neurons_hidden=[num_of_neruons_hidden],
                              num_of_classes=num_of_classes))

        train(network, training[0], training[1], validation[0], validation[1], batch_size=batch_size, epochs=epochs_for_all, learning_rate=lr)