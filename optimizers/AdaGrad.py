from optimizers.Optimizer import Optimizer

import numpy as np

class AdaGrad(Optimizer):
    def __init__(self, learning_rate, epsilon, num_of_inputs, num_of_neurons_hidden, num_of_classes):
        super().__init__(learning_rate, num_of_inputs, num_of_neurons_hidden, num_of_classes)
        self.epsilon = epsilon

        self.W_adagrad_acc_list = np.zeros(shape=len(num_of_neurons_hidden)+1)
        self.b_adagrad_acc_list = np.zeros(shape=len(num_of_neurons_hidden)+1)

    def update_weights(self, W_list, b_list, delta, index, shape, output):
        W_gradient = output.T @ delta / shape
        b_gradient = delta.T @ (np.ones(shape)) / shape
        self.W_adagrad_acc_list[index] += np.sum(W_gradient ** 2)
        self.b_adagrad_acc_list[index] += np.sum(b_gradient ** 2)
        W_list[index] += ((self.learning_rate / np.sqrt(self.W_adagrad_acc_list[index] + self.epsilon)) * W_gradient)
        b_list[index] += ((self.learning_rate / np.sqrt(self.b_adagrad_acc_list[index] + self.epsilon)) * b_gradient)

    def get_name(self):
        return "AdaGrad"