from optimizers.Optimizer import Optimizer

import numpy as np


class SGD(Optimizer):
    def __init__(self, learning_rate, num_of_inputs, num_of_neurons_hidden, num_of_classes):
        super().__init__(learning_rate, num_of_inputs, num_of_neurons_hidden, num_of_classes)

    def update_weights(self, W_list, b_list, delta, index, shape, output):
        W_list[index] += (self.learning_rate * (output.T @ delta / shape))
        b_list[index] += (self.learning_rate * (delta.T @ (np.ones(shape)) / shape))

    def get_name(self):
        return "SGD"
