import math

from optimizers.Optimizer import Optimizer

import numpy as np


class Adam(Optimizer):
    def __init__(self, learning_rate, beta1, beta2, epsilon, num_of_inputs, num_of_neurons_hidden, num_of_classes):
        super().__init__(learning_rate, num_of_inputs, num_of_neurons_hidden, num_of_classes)
        self.beta1 = beta1
        self.beta2 = beta2
        self.epsilon = epsilon

        self.m_W = []
        self.v_W = []
        self.m_b = []
        self.v_b = []

        self.m_W.append(np.zeros((num_of_inputs, num_of_neurons_hidden[0])))
        self.v_W.append(np.zeros((num_of_inputs, num_of_neurons_hidden[0])))

        for i in range(len(num_of_neurons_hidden) - 1):
            self.m_W.append(np.zeros((num_of_neurons_hidden[i], num_of_neurons_hidden[i + 1])))
            self.v_W.append(np.zeros((num_of_neurons_hidden[i], num_of_neurons_hidden[i + 1])))
            self.m_b.append(np.zeros((num_of_neurons_hidden[i])))
            self.v_b.append(np.zeros((num_of_neurons_hidden[i])))

        self.m_W.append(np.zeros((num_of_neurons_hidden[-1], num_of_classes)))
        self.v_W.append(np.zeros((num_of_neurons_hidden[-1], num_of_classes)))
        self.m_b.append(np.zeros(num_of_neurons_hidden[-1]))
        self.m_b.append(np.zeros(num_of_classes))
        self.v_b.append(np.zeros(num_of_neurons_hidden[-1]))
        self.v_b.append(np.zeros(num_of_classes))

        self.t = np.zeros(shape=len(num_of_neurons_hidden) + 1)

    def update_weights(self, W_list, b_list, delta, index, shape, output):
        W_gradient = output.T @ delta / shape
        b_gradient = delta.T @ (np.ones(shape)) / shape
        self.t[index] += 1
        self.m_W[index] = self.beta1 * self.m_W[index] + (1 - self.beta1) * W_gradient
        self.v_W[index] = self.beta2 * self.v_W[index] + (1 - self.beta2) * (W_gradient ** 2)

        self.m_b[index] = self.beta1 * self.m_b[index] + (1 - self.beta1) * b_gradient
        self.v_b[index] = self.beta2 * self.v_b[index] + (1 - self.beta2) * (b_gradient ** 2)

        m_W_dash = self.m_W[index] / (1 - (self.beta1 ** self.t[index]))
        v_W_dash = self.v_W[index] / (1 - (self.beta2 ** self.t[index]))

        m_b_dash = self.m_b[index] / (1 - (self.beta1 ** self.t[index]))
        v_b_dash = self.v_b[index] / (1 - (self.beta2 ** self.t[index]))

        W_list[index] += self.learning_rate / (np.sqrt(v_W_dash) + self.epsilon)*m_W_dash
        b_list[index] += self.learning_rate / (np.sqrt(v_b_dash) + self.epsilon)*m_b_dash

    def get_name(self):
        return "Adam"

