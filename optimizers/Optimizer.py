class Optimizer:
    def __init__(self, learning_rate, num_of_inputs, num_of_neurons_hidden, num_of_classes):
        self.learning_rate = learning_rate
        self.num_of_inputs = num_of_inputs
        self.num_of_neurons_hidden = num_of_neurons_hidden
        self.num_of_classes = num_of_classes

    def update_weights(self, W_list, b_list, delta, index, shape, output):
        return NotImplemented

    def get_name(self):
        return NotImplemented

