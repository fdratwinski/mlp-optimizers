from optimizers.Optimizer import Optimizer

import numpy as np


class AdaDelta(Optimizer):
    def __init__(self, learning_rate, gamma, epsilon, num_of_inputs, num_of_neurons_hidden, num_of_classes):
        super().__init__(learning_rate, num_of_inputs, num_of_neurons_hidden, num_of_classes)
        self.epsilon = epsilon
        self.gamma = gamma

        self.W_adadelta_gradient_list = []
        self.W_adadelta_step_list = []
        self.W_changes = []
        self.b_adadelta_gradient_list = []
        self.b_adadelta_step_list = []
        self.b_changes = []

        self.W_adadelta_gradient_list.append(np.zeros((num_of_inputs, num_of_neurons_hidden[0])))
        self.W_adadelta_step_list.append(np.zeros((num_of_inputs, num_of_neurons_hidden[0])))
        self.W_changes.append(np.zeros((num_of_inputs, num_of_neurons_hidden[0])))

        for i in range(len(num_of_neurons_hidden) - 1):
            self.W_adadelta_gradient_list.append(np.zeros((num_of_neurons_hidden[i], num_of_neurons_hidden[i + 1])))
            self.W_adadelta_step_list.append(np.zeros((num_of_neurons_hidden[i], num_of_neurons_hidden[i + 1])))
            self.W_changes.append(np.zeros((num_of_neurons_hidden[i], num_of_neurons_hidden[i + 1])))
            self.b_adadelta_gradient_list.append(np.zeros((num_of_neurons_hidden[i])))
            self.b_adadelta_step_list.append(np.zeros((num_of_neurons_hidden[i])))
            self.b_changes.append(np.zeros((num_of_neurons_hidden[i])))

        self.W_adadelta_gradient_list.append(np.zeros((num_of_neurons_hidden[-1], num_of_classes)))
        self.W_adadelta_step_list.append(np.zeros((num_of_neurons_hidden[-1], num_of_classes)))
        self.W_changes.append(np.zeros((num_of_neurons_hidden[-1], num_of_classes)))
        self.b_adadelta_gradient_list.append(np.zeros(num_of_neurons_hidden[-1]))
        self.b_adadelta_gradient_list.append(np.zeros(num_of_classes))
        self.b_adadelta_step_list.append(np.zeros(num_of_neurons_hidden[-1]))
        self.b_adadelta_step_list.append(np.zeros(num_of_classes))
        self.b_changes.append(np.zeros(num_of_neurons_hidden[-1]))
        self.b_changes.append(np.zeros(num_of_classes))

    def update_weights(self, W_list, b_list, delta, index, shape, output):
        W_gradient = output.T @ delta / shape
        b_gradient = delta.T @ (np.ones(shape)) / shape

        self.W_adadelta_gradient_list[index] += self.gamma * self.W_adadelta_gradient_list[index] + (1. - self.gamma) * np.square(W_gradient)
        self.b_adadelta_gradient_list[index] += self.gamma * self.b_adadelta_gradient_list[index] + (1. - self.gamma) * np.square(b_gradient)

        self.W_changes[index] = np.sqrt(self.W_adadelta_step_list[index] + self.epsilon) / np.sqrt(self.W_adadelta_step_list[index] + self.epsilon) * W_gradient
        self.b_changes[index] = np.sqrt(self.b_adadelta_step_list[index] + self.epsilon) / np.sqrt(self.b_adadelta_gradient_list[index] + self.epsilon) * b_gradient
        W_list[index] += self.W_changes[index]
        b_list[index] += self.b_changes[index]

        self.W_adadelta_step_list[index] = self.gamma * self.W_adadelta_step_list[index] + (1. - self.gamma) * self.W_changes[index] * self.W_changes[index]
        self.b_adadelta_step_list[index] = self.gamma * self.b_adadelta_step_list[index] + (1. - self.gamma) * self.b_changes[index] * self.b_changes[index]

    def get_name(self):
        return "AdaDelta"